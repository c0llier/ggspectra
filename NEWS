ggspectra 0.1.8
===============

Add support for plot.qty = "absorptance" to plot.filter_spct()
Add support for plot.qty to plot.object_spct() with possible values: "all" or
NULL (previous behaviour, and current default), "transmittance", "absorbance",
"absorptance", and "reflectance". The new options plot only one quantity at
a time as a line instead of the default area plot.

ggspectra 0.1.7
===============

Add stat_wb_label(), stat_wb_e_irrad(), stat_wb_q_irrad(), stat_wb_e_sirrad(),
and stat_wb_q_irrad().
Revise plot.raw_spct() to handle multiple "counts" columns.
Revise plot.cps_spct() to handle multiple "cps" columns.
Add text.size parameter to all plot() methods. Can be used to control the size
of the font used for text decorations related to wavebands, peaks and valleys.
Increase slightly the default font size for plot decorations, and decrease the
number of peaks and valleys labelled by the plot() methods.
Fix existing bug revealed by dplyr update.
Fix bug in decoration() affecting plot.objects_spct().
Fix other minor bugs.

ggspectra 0.1.6
===============

Clean documentation and code for CRAN submission.
Add examples of plotting of waveband objects as these were removed from
the vignette of package 'photobiologyWavebands'.

ggspectra 0.1.5
===============

Fix bug in documentation.

ggspectra 0.1.4
===============

Improve documentation.

ggspectra 0.1.3
===============

Remove wb_guide()
Remove wb_wb_summary()
Add stat_wb_mean(), stat_wb_total(), stat_wb_contribution(), stat_wb_relative()
Add stat_wb_irrad(), stat_wb_sirrad()

Implement 100% of the old functionality of the plot() methods, but without using
any annotations. In other words, now all calculations work as expected with
grouping and panels. Some aesthetics may require tweaking in the case of
grouping.

Tested with packages ggrepel and cowplot.

ggspectra 0.1.2
===============

Add text explaining examples to User Guide.

Rename several stats and functions with more descriptive names.

stat_color_guide() -> stat_wl_strip()
stat_average() -> stat_wl_summary()
stat_waveband() -> stat_wb_summary()
color_guide() -> wl_guide()
waveband_guide() -> wb_guide()

ggspectra 0.1.1
===============

Add multiplot() function (unchanged from package photobiologygg).
Improve stat_waveband(). Add color_guide(), waveband_guide().

Add ggplot() methods for spectra, which use default mappings if mapping not
explicitly supplied as argument to call.

Make plot() methods almost fully functional. "contribution" and "participation"
summary values not yet implemented. Neither summaries for BSWFs. If requested
these summaries will show as "NaN" (not a number) in the plots.

ggspectra 0.1.0
===============

First preview version. Still with incomplete functionality.