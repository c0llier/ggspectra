sd_section("Package overview", "",
           c(
             "ggspectra-package"
             )
)

sd_section("Geoms",
  "Geoms, short for geometric objects, describe the type of plot you will produce.",
  c(
    "geom_spct"
    )
)

sd_section("Statistics",
  "It's often useful to transform your data before plotting, and that's what statistical transformations do.",
  c(
    "stat_color",
    "stat_peaks",
    "stat_wb_contribution",
    "stat_wb_irrad",
    "stat_wb_label",
    "stat_wb_mean",
    "stat_wb_relative",
    "stat_wb_sirrad",
    "stat_wb_total",
    "stat_wl_strip",
    "stat_wl_summary"
  )
)

sd_section("Plot creation", "",
  c(
    "ggplot",
    "plot.raw_spct",
    "plot.cps_spct",
    "plot.source_spct",
    "plot.response_spct",
    "plot.filter_spct",
    "plot.reflector_spct",
    "plot.object_spct",
    "plot.waveband",
    "multiplot"
    )
)
