# ggspectra #

Package '**ggspectra**' provides a set of _stats_, _geoms_ and _methods_ to be used as extensions to packages '**ggplot2**' and '**photobiology**'. They easy the task of plotting radiation-related spectra and of annotating the resulting plots with labels and summary quantities derived from the spectral data. 

This package is part of a suite of R packages for photobiological calculations described at the [r4photobiology](http://www.r4photobiology.info) web site.
